package de.griffel.confluence.plugins.plantuml.config;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import net.sourceforge.plantuml.OptionFlags;
import org.apache.commons.io.input.ClassLoaderObjectInputStream;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * This class is responsible for loading and storing the configuration for this plugin.
 */
public final class DefaultPlantUmlConfigurationManager implements PlantUmlConfigurationManager {

   private static final Logger log = LoggerFactory
     .getLogger(DefaultPlantUmlConfigurationManager.class);

   private final BandanaManager bandanaManager;

   public DefaultPlantUmlConfigurationManager(BandanaManager bandanaManager) {
      this.bandanaManager = bandanaManager;
   }

   static {
      OptionFlags.ALLOW_INCLUDE = false;
      if ("true".equalsIgnoreCase(System.getenv("ALLOW_PLANTUML_INCLUDE"))) {
         OptionFlags.ALLOW_INCLUDE = true;
      }
   }

   public PlantUmlConfiguration load() {
      final ConfluenceBandanaContext context = new ConfluenceBandanaContext();

      PlantUmlConfiguration config;
      Serializable storedConfig = (Serializable) bandanaManager.getValue(
        context, PlantUmlConfigurationBean.class.getName());

      if (storedConfig == null) {
         config = new PlantUmlConfigurationBean();
      } else if (storedConfig instanceof PlantUmlConfiguration) {
        config = (PlantUmlConfiguration) storedConfig;
      } else {
         // https://jira.atlassian.com/browse/CONFSERVER-9356
         // https://jira.atlassian.com/browse/CONFSERVER-62966
         log.info("Recreating plantuml configuration for current plugin classloader");
         config = recreateInCurrentClassLoader(storedConfig);
      }
      return config;
   }

   public void save(PlantUmlConfiguration config) {
      final ConfluenceBandanaContext context = new ConfluenceBandanaContext();
      bandanaManager.setValue(context, PlantUmlConfigurationBean.class.getName(), config);
   }

   @VisibleForTesting
   static PlantUmlConfiguration recreateInCurrentClassLoader(Serializable storedConfig) {
      final byte[] bytes = SerializationUtils.serialize(storedConfig);
      final PlantUmlConfiguration config = deserialize(PlantUmlConfiguration.class.getClassLoader(), bytes);
      return config;
   }

   private static <T extends Serializable> T deserialize(ClassLoader classLoader, byte[] bytes) {
      try {
         final ObjectInputStream inputStream = new ClassLoaderObjectInputStream(classLoader, new ByteArrayInputStream(bytes));
         @SuppressWarnings("unchecked")
         final T object = (T) inputStream.readObject();
         return object;
      } catch (IOException | ClassNotFoundException e) {
         throw new RuntimeException("Cannot read plantuml configuration from bandana", e);
      }
   }

}

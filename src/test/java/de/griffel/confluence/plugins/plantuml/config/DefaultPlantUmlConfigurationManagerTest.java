package de.griffel.confluence.plugins.plantuml.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.Serializable;
import java.net.URLClassLoader;
import org.apache.commons.beanutils.PropertyUtils;
import org.junit.jupiter.api.Test;

class DefaultPlantUmlConfigurationManagerTest {
  @Test
  void testRecreateInCurrentClassLoader() throws Exception {
    ClassLoader oldClassLoader = new URLClassLoader(
      ((URLClassLoader) this.getClass().getClassLoader()).getURLs(), null);
    Serializable oldConfiguration = (Serializable) oldClassLoader.loadClass(PlantUmlConfigurationBean.class.getName()).newInstance();
    PropertyUtils.setProperty(oldConfiguration, "commonFooter", "someFooter");

    assertFalse(
      PlantUmlConfiguration.class.isInstance(oldConfiguration)
    );

    Object recreatedConfiguration = DefaultPlantUmlConfigurationManager.recreateInCurrentClassLoader(oldConfiguration);
    assertTrue(
      PlantUmlConfiguration.class.isInstance(recreatedConfiguration),
      "Object should have been recreated in current classloader"
    );
    PlantUmlConfiguration config = (PlantUmlConfiguration) recreatedConfiguration;
    assertEquals(
      config.getCommonFooter(),
      "someFooter"
    );
  }
}